<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function posts()
    {
    								// Modelo, Tabla(alphabet order), Modelo Actual, Modelo a unir
    	return $this->belongsToMany('App\Post', 'post_tag', 'tag_id', 'post_id');
    }
}
