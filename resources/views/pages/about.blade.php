@extends('main')

@section('title', '| About Me')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <h1>About Me</h1>
                <h4>{{ $data['fullname'] }} </h4>
                <h5>{{ $data['email'] }}</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur voluptate esse minus minima fuga ab qui error rerum officia voluptatum placeat, doloribus sit, accusamus unde odit repellat. Eligendi, illum, excepturi.</p>
            </div>
        </div>
@endsection