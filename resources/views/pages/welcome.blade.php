@extends('main')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                  <h1>Welcome to LaraBlog!</h1>
                  <p>Thank you so much for visiting. This is my test website build with Laravel 5.2</p>
                  <p><a class="btn btn-primary btn-lg" href="#" role="button">Popular Post</a></p>
                </div>
            </div>
        </div> <!-- End of header .row -->

        <div class="row">
            <div class="col-md-8">
            @foreach($posts as $post)
                <div class="post">
                    <h3>{{ $post->title }}</h3>
                    <p>{{ substr($post->body, 0, 300) }} {{ strlen($post->body) > 300 ? "..." : "" }}</p>
                    <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">Read More</a>
                </div>

                <hr>

                @endforeach
            </div>
            <div class="col-md-3 col-md-offset-1">
                <h2>Sidebar</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui esse aliquid eos earum excepturi a facilis nam quia recusandae, fugit, vero nemo, autem! Natus ducimus qui, corporis totam distinctio aliquid.</p>
            </div>
        </div><!-- End of header .row -->
@endsection